package com.thetale.app.consts;

/**
 * Created by Rim4i4ok on 5/7/14.
 */
public class StringConstants {

    public static final String TOKEN = "X-CSRFToken";
    public static final String SESSION_ID = "sessionid";

    public static final String UTF_8 = "UTF-8";
    public static final String OPTION_UID = "option_uid";

    public static final String APP_PREFERENCES = "mysettings";
    public static final String APP_PREFERENCES_LOGIN = "login";
    public static final String APP_PREFERENCES_PASSWORD = "password";

    public static final String BASE_URL = "http://the-tale.org";

    private static final String API_VERSION10 = "api_version=1.0";
    private static final String API_VERSION11 = "api_version=1.1";
    private static final String CLIENT_VERSION = "api_client=tta-1.0";

    public static final String INIT_URL = BASE_URL + "/game/api/info" + "?" + API_VERSION10 + "&" + CLIENT_VERSION;
    public static final String CONNECT_URL = BASE_URL + "/accounts/auth/api/login" + "?" + API_VERSION10 + "&" + CLIENT_VERSION;
    public static final String INFO_URL = BASE_URL + "/game/api/info"+ "?" + API_VERSION11 + "&" + CLIENT_VERSION;
    public static final String EXIT_URL = BASE_URL + "/accounts/auth/api/logout"+ "?" + API_VERSION10 + "&" + CLIENT_VERSION;
    public static final String HELP_URL = BASE_URL + "/game/abilities/help/api/use" + "?" + API_VERSION10 + "&" + CLIENT_VERSION;
    public static final String MAKE_CHOICE_URL = BASE_URL + "/game/quests/api/choose" + "?" + API_VERSION10 + "&" + CLIENT_VERSION;

}
