package com.thetale.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.thetale.app.application.MyApplication;
import com.thetale.app.consts.StringConstants;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.http.ConnectionDetector;
import com.thetale.app.interfaces.OnTaskCompleted;
import com.thetale.app.task.ConnectToServerTask;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity implements OnTaskCompleted {

    Context _context;
    ConnectionDetector _detector;

    SharedPreferences mSettings;

    EditText login;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        mSettings = getSharedPreferences(StringConstants.APP_PREFERENCES, Context.MODE_PRIVATE);
        _context = getApplicationContext();

        login = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);

        String log = mSettings.getString(StringConstants.APP_PREFERENCES_LOGIN, "");
        String pass = mSettings.getString(StringConstants.APP_PREFERENCES_PASSWORD, "");

        login.setText(log);
        password.setText(pass);

        _detector = new ConnectionDetector(_context);

        // Если уже раньше заходили, то входим автоматически
        if(log != "" && pass != ""){
            Connect();
        }
    }

    // Клик кнопки вход
    public void LogIn(View v){
        Connect();
    }

    // Авторизация
    private void Connect(){
        if(_detector.isConnectingToInternet()) {
            connectToServer();
        }
        else{
            showInternetAlert();
        }
    }

    // Окошко с предупреждением об отсутствии интернета
    private void showInternetAlert(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Интернет");

        // Setting Dialog Message
        alertDialog.setMessage("Отсутствует подключение к интернету");

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
            }
        });

        alertDialog.setIcon(R.drawable.game_icon);
        // Showing Alert Message
        alertDialog.show();
    }

    // Регистрация
    public void Register(View v){
        Uri uri = Uri.parse(StringConstants.BASE_URL);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    // Подключение к серверу
    public void connectToServer(){
        try {
            String l = login.getText().toString();
            String p = password.getText().toString();

            new ConnectToServerTask(this, new MyApplication(), mSettings, this).execute(l, p);
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

    public void onTaskCompleted(Boolean value){
        if(value){

            mSettings.edit().putString(StringConstants.APP_PREFERENCES_LOGIN, login.getText().toString()).commit();
            mSettings.edit().putString(StringConstants.APP_PREFERENCES_PASSWORD, password.getText().toString()).commit();

            Intent i = new Intent(_context, MainActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
        else{
            new AlertDialog.Builder(this)
                    .setTitle("Ошибка авторизации")
                    .setMessage("Возможно вы ввели не правильные данные?")
                    .setPositiveButton("Закрыть", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }
}