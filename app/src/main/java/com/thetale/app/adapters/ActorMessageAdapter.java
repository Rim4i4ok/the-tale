package com.thetale.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.objects.Actor;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 24.05.2014.
 */
public class ActorMessageAdapter extends ArrayAdapter<Actor> {

    public ArrayList<Actor> items;
    private Context context;

    public ActorMessageAdapter(Context context, int textViewResourceId, ArrayList<Actor> items){
        super(context, textViewResourceId, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;

        if(v == null){
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.actor_row, null);
        }

        Actor record = items.get(position);

        try {

            // Тип участника
            TextView actor_type = (TextView) v.findViewById(R.id.actor_type);
            if (actor_type != null) {
                actor_type.setText(record.getActorName());
            }

            // Название участника
            TextView actor_name = (TextView) v.findViewById(R.id.actor_name);
            if (actor_name != null) {
                if (isNullOrEmpty(record.getGoal())) {
                    actor_name.setText(record.getName());
                } else {
                    actor_name.setText(record.getGoal());
                }
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return v;
    }

    private Boolean isNullOrEmpty(String str){

        Boolean result = false;

        if(str == null || str == "null"){
            result = true;
        }else{
            result = false;
        }

        return result;
    }

}
