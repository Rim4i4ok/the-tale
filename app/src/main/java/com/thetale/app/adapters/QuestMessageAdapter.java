package com.thetale.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.thetale.app.MainActivity;
import com.thetale.app.R;
import com.thetale.app.application.MyApplication;
import com.thetale.app.enums.EnumQuest;
import com.thetale.app.images.UIQuests;
import com.thetale.app.objects.Actor;
import com.thetale.app.objects.Choice;
import com.thetale.app.objects.QuestRecord;
import com.thetale.app.task.HelpHeroTask;
import com.thetale.app.task.MakeChoiceTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rim4i4ok on 4/20/14.
 */
public class QuestMessageAdapter extends ArrayAdapter<QuestRecord> {

    public ArrayList<QuestRecord> items;
    private Context context;

    public QuestMessageAdapter(Context context, int textViewResourceId, ArrayList<QuestRecord> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.quest_row, null);
        }
        QuestRecord record = items.get(position);

        if (record != null) {

            // Картинка квеста
            ImageView quest_image = (ImageView) v.findViewById(R.id.quest_img);
            if (quest_image != null) {
                quest_image.setImageBitmap(EnumQuest.getImage(record.getType()));
            }

            // Название квеста
            TextView quest_name = (TextView) v.findViewById(R.id.quest_name);
            if (quest_name != null) {
                quest_name.setText(record.getName());
            }

            // Его опыт и влияние
            TextView quest_power = (TextView) v.findViewById(R.id.quest_power);
            if (quest_power != null) {
                if (record.getExperience() != 0 || record.getPower() != 0) {
                    quest_power.setText("(" + "опыт: +" + record.getExperience() + ", влияние: +" + record.getPower() + ")");
                } else {
                    quest_power.setVisibility(View.GONE);
                }
            }

            // Текущее решение
            TextView quest_choice = (TextView) v.findViewById(R.id.quest_choice);
            if (quest_choice != null) {
                if (record.getChoice().equalsIgnoreCase("null")) {
                    quest_choice.setVisibility(View.GONE);
                } else {
                    quest_choice.setText(record.getChoice());
                }
            }

            // Список участников
            LinearLayout ll_actors = (LinearLayout) v.findViewById(R.id.ll_actors);
            if(ll_actors != null) {
                ArrayList<Actor> actors = record.getActors();
                for (int i = 0; i < actors.size(); i++) {
                    ListView list_view_actor = new ListView(context);

                    ArrayList<Actor> actor = new ArrayList<Actor>();
                    actor.add(actors.get(i));

                    ActorMessageAdapter adapter = new ActorMessageAdapter(context.getApplicationContext()
                            , R.layout.actor_row, actor);

                    list_view_actor.setAdapter(adapter);
                    ll_actors.addView(list_view_actor);
                }
            }

            // Возможный выбор в задании
            LinearLayout ll_alternatives = (LinearLayout)v.findViewById(R.id.ll_alternatives);
            if(ll_alternatives != null){
                List<Choice> alternatives = record.getChoice_alternatives();
                for(int i = 0; i < alternatives.size(); i++){

                    Choice choice = alternatives.get(i);

                    Button button = new Button(context);
                    button.setText(choice.getDescription());
                    button.setTag(choice.getUid());
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String tag = view.getTag().toString();
                            new MakeChoiceTask(new MyApplication()).execute(tag);

                            //view.setVisibility(View.GONE);
                            //View v = (View)view.getParent();
                            //v.setVisibility(View.GONE);
                        }

                    });

                    ll_alternatives.addView(button);
                }
            }
        }
        return v;
    }
}
