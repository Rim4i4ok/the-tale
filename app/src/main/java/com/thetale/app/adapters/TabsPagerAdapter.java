package com.thetale.app.adapters;

import android.database.DataSetObserver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.thetale.app.enums.EnumFragment;
import com.thetale.app.fragments.Bag;
import com.thetale.app.fragments.Diary;
import com.thetale.app.fragments.Equipment;
import com.thetale.app.fragments.Journal;
import com.thetale.app.fragments.Player;
import com.thetale.app.fragments.Quest;

/**
 * Created by Rim4i4ok on 24.05.2014.
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    public TabsPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        if (observer != null) {
            super.unregisterDataSetObserver(observer);
        }
    }

    @Override
    public Fragment getItem(int index){

        Fragment result = null;

        switch (index){
            case EnumFragment.PLAYER:
                result = new Player();
                break;
            case EnumFragment.DIARY:
                result = new Diary();
                break;
            case EnumFragment.JOURNAL:
                result = new Journal();
                break;
            case EnumFragment.QUESTS:
                result = new Quest();
                break;
            case EnumFragment.EQUIPMENT:
                result = new Equipment();
                break;
            case EnumFragment.BAG:
                result = new Bag();
                break;
            default:
                result = new Player();
                break;
        }

        return result;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return EnumFragment.getCount();
    }

}
