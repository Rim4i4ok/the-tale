package com.thetale.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.objects.ErrorRecord;
import com.thetale.app.objects.JournalRecord;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 3/30/14.
 */
public class ErrorMessageAdapter extends ArrayAdapter<ErrorRecord> {

    public ArrayList<ErrorRecord> items;
    private Context context;

    public ErrorMessageAdapter(Context context, int textViewResourceId, ArrayList<ErrorRecord> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.error_row, null);
        }
        ErrorRecord record = items.get(position);
        if (record != null) {
            TextView date = (TextView) v.findViewById(R.id.time);
            TextView message = (TextView) v.findViewById(R.id.message);
            if (date != null) {
                date.setText(record.getDate().toString());
            }
            if(message != null){
                message.setText(record.getMessage());
            }
        }
        return v;
    }

}
