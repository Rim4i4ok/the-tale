package com.thetale.app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.enums.EnumEquipment;
import com.thetale.app.enums.EnumRarity;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.objects.EquipmentRecord;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 4/19/14.
 */
public class EquipmentMessageAdapter extends ArrayAdapter<EquipmentRecord> {

    public ArrayList<EquipmentRecord> items;
    private Context context;

    public EquipmentMessageAdapter(Context context, int textViewResourceId, ArrayList<EquipmentRecord> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.equipment_row, null);
        }
        EquipmentRecord record = items.get(position);
        if (record != null) {

            try{
                ImageView image = (ImageView) v.findViewById(R.id.equip_img);
                // Картинка экипировки
                if(image != null){
                    image.setImageBitmap(EnumEquipment.getImage(record.getType()));
                }

                TextView equip_name = (TextView) v.findViewById(R.id.equip_name);
                // Название экипировки
                if (equip_name != null) {
                    equip_name.setText(record.getName());
                    equip_name.setTextColor(context.getResources().getColor(EnumRarity.getColor(record.getRarity())));
                }

                TextView power = (TextView) v.findViewById(R.id.power_power);
                // Физическая мощность экипировки
                if(power != null){
                    power.setText(Integer.toString(record.getPower()));
                }

                TextView magic = (TextView)v.findViewById(R.id.power_magic);
                // Магищеская мощность экипировки
                if(magic != null){
                    magic.setText(Integer.toString(record.getMagic()));
                }
            }
            catch (Exception exception){
                ErrorsList.AddError(exception.toString());
            }
        }
        return v;
    }

}
