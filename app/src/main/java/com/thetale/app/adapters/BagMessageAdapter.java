package com.thetale.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.enums.EnumArtifact;
import com.thetale.app.enums.EnumEquipment;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.objects.BagRecord;
import com.thetale.app.objects.EquipmentRecord;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 4/20/14.
 */
public class BagMessageAdapter extends ArrayAdapter<BagRecord> {

    public ArrayList<BagRecord> items;
    private Context context;

    public BagMessageAdapter(Context context, int textViewResourceId, ArrayList<BagRecord> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.bag_row, null);
        }
        BagRecord record = items.get(position);
        if (record != null) {

            try{
                // Картинка предмета в рюкзаке
                ImageView bag_img = (ImageView) v.findViewById(R.id.bag_img);
                if(bag_img != null){
                    bag_img.setImageBitmap(EnumArtifact.getImage(record.getType()));
                }

                // Название предмета в рюкзаке
                TextView bag_name = (TextView) v.findViewById(R.id.bag_name);
                if(bag_name != null){
                    bag_name.setText(record.getName());
                }
            }
            catch (Exception exception){
                ErrorsList.AddError(exception.toString());
            }
        }
        return v;
    }

}
