package com.thetale.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.objects.DiaryRecord;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 3/23/14.
 */
public class DiaryMessageAdapter extends ArrayAdapter<DiaryRecord> {

    public ArrayList<DiaryRecord> items;
    private Context context;

    public DiaryMessageAdapter(Context context, int textViewResourceId, ArrayList<DiaryRecord> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.diary_row, null);
        }
        DiaryRecord record = items.get(position);

        if (record != null) {
            TextView date = (TextView) v.findViewById(R.id.time);
            if (date != null) {
                date.setText("Дата: " + record.getTime() + " " + record.getDate() + ".");
            }

            TextView message = (TextView) v.findViewById(R.id.message);
            if(message != null){
                message.setText(record.getMessage());
            }
        }
        return v;
    }

}
