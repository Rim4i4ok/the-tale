package com.thetale.app.errors;

import com.thetale.app.objects.ErrorRecord;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 3/30/14.
 */
public class ErrorsList {

    private static ArrayList<ErrorRecord> _errors = new ArrayList<ErrorRecord>();

    public static ArrayList<ErrorRecord> getErrors() {
        return _errors;
    }

    public static void AddError(String message){
        _errors.add(new ErrorRecord(message));
    }

}
