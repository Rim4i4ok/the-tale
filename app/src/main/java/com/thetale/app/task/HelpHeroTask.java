package com.thetale.app.task;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.thetale.app.MainActivity;
import com.thetale.app.application.MyApplication;
import com.thetale.app.consts.StringConstants;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.fragments.Player;
import com.thetale.app.http.HttpClientFactory;
import com.thetale.app.interfaces.OnTaskCompleted;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rim4i4ok on 5/7/14.
 */
public class HelpHeroTask extends AsyncTask<Void, Void, Boolean> {

    private ProgressDialog dialog;
    private MyApplication application;
    private OnTaskCompleted listener;

    public HelpHeroTask(MainActivity activity, MyApplication application, OnTaskCompleted listener){
        this.dialog = new ProgressDialog(activity);
        this.application = application;
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(Void... params){
        Boolean result = true;

        byte[] bytes = null;
        String str = "";

        try {
            // Создадим HttpClient и PostHandler
            DefaultHttpClient httpClient = application.getHttpClient();
            HttpPost httpPost = new HttpPost(StringConstants.HELP_URL);
            httpPost.addHeader(StringConstants.TOKEN, MyApplication.getToken());

            // Выполним запрос
            HttpResponse response = httpClient.execute(httpPost);

            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpURLConnection.HTTP_OK){
                bytes = EntityUtils.toByteArray(response.getEntity());
                result = true;
                str = new String(bytes, StringConstants.UTF_8);
            }else {
                result = false;
                str = statusLine.toString();
            }
        } catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        GetInfo.GetInfo(application);

        return result;
    }

    @Override
    protected void onPreExecute(){
        dialog.setMessage("Помогаем");
        dialog.show();
    }

    protected void onPostExecute(Boolean o){
        if(dialog.isShowing()){
            dialog.dismiss();
        }
        listener.onTaskCompleted(o);
    }

}
