package com.thetale.app.task;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.thetale.app.MainActivity;
import com.thetale.app.application.MyApplication;
import com.thetale.app.consts.StringConstants;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.interfaces.OnTaskCompleted;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rim4i4ok on 07.06.2014.
 */
public class MakeChoiceTask extends AsyncTask<String, Void, Boolean> {

    private MyApplication application;

    public MakeChoiceTask(MyApplication application){
        this.application = application;;
    }

    @Override
    protected Boolean doInBackground(String... params){
        Boolean result = true;

        byte[] bytes = null;
        String str = "";

        try {
            // Создадим HttpClient и PostHandler
            DefaultHttpClient httpClient = application.getHttpClient();
            String url = StringConstants.MAKE_CHOICE_URL + "&option_uid=" + URLEncoder.encode(params[0], "UTF-8");
            HttpPost httpPost = new HttpPost(url);
            httpPost.addHeader(StringConstants.TOKEN, MyApplication.getToken());

            // Выполним запрос
            HttpResponse response = httpClient.execute(httpPost);

            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == HttpURLConnection.HTTP_OK){
                bytes = EntityUtils.toByteArray(response.getEntity());
                result = true;
                str = new String(bytes, StringConstants.UTF_8);
            }else {
                result = false;
                str = statusLine.toString();
            }
        } catch (Exception exception){
            exception.printStackTrace();
            //ErrorsList.AddError(exception.toString());
        }

        GetInfo.GetInfo(application);

        return result;
    }

    @Override
    protected void onPreExecute(){

    }

    protected void onPostExecute(Boolean o){
    }

}
