package com.thetale.app.task;

import android.content.Context;

import com.thetale.app.application.MyApplication;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.http.ConnectionDetector;
import com.thetale.app.http.HttpClientFactory;
import com.thetale.app.json.JSONParse;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Created by Rim4i4ok on 3/30/14.
 */
public class GetInfoThread implements Runnable {

    private MyApplication _application;
    private Context _context;
    private ConnectionDetector _detector;

    public GetInfoThread(MyApplication application, Context context){
        this._application = application;
        this._context = context;
    }

    @Override
    public void run(){
        try{
            _detector = new ConnectionDetector(_context);
            while (true){

                // Через 10 секунд еще раз получаем данные
                Thread.sleep(10000);
                if(_detector.isConnectingToInternet()) {
                    GetInfo.GetInfo(_application);
                }
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

}
