package com.thetale.app.task;

import android.os.AsyncTask;

import com.thetale.app.application.MyApplication;
import com.thetale.app.consts.StringConstants;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.http.HttpClientFactory;
import com.thetale.app.interfaces.OnTaskCompleted;
import com.thetale.app.json.JSONParse;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import java.net.HttpURLConnection;

/**
 * Created by Rim4i4ok on 3/19/14.
 */
public class DisConnectFromServer extends AsyncTask<String, Void, Boolean> {

    private MyApplication application;
    private OnTaskCompleted listener;

    public DisConnectFromServer(MyApplication application, OnTaskCompleted listener){
        this.application = application;
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try{
            HttpPost httppost = new HttpPost(StringConstants.EXIT_URL);

            BasicHttpContext mHttpContext = application.getHttpContext();
            DefaultHttpClient httpclient = application.getNewHttpClient();

            String token = application.getCookieStore().getCookies().get(0).getValue();

            httppost.addHeader("X-CSRFToken", token);

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost, mHttpContext);

            JSONParse.setConnectAnswer("");

            return true;
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return false;
    }

    protected void onPostExecute(Boolean o){
        listener.onTaskCompleted(o);
    }

}
