package com.thetale.app.task;

import com.thetale.app.json.JSONData;
import com.thetale.app.application.MyApplication;
import com.thetale.app.consts.StringConstants;
import com.thetale.app.errors.ErrorsList;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Created by Rim4i4ok on 5/7/14.
 */
public class GetInfo {

    // Получение информации о персонаже
    public static Boolean GetInfo(MyApplication application){

        Boolean result = false;

        try{
            HttpGet httpGet = new HttpGet(StringConstants.INFO_URL);
            DefaultHttpClient httpClient = application.getHttpClient();
            httpClient.setCookieStore(application.getCookieStore());

            String token = application.getCookieStore().getCookies().get(0).getValue();
            httpGet.addHeader(StringConstants.TOKEN, token);
            String sessionId = application.getCookieStore().getCookies().get(1).getValue();
            httpGet.addHeader(StringConstants.SESSION_ID, sessionId);

            HttpResponse response = httpClient.execute(httpGet);

            if(response.getStatusLine().getStatusCode() == 200){
               String json = EntityUtils.toString(response.getEntity());

                JSONData jd = JSONData.getInstance();
                jd.setData(json);
                result = true;
            }
            else{
                result = false;
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }
        finally {
            return result;
        }
    }

}
