package com.thetale.app.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.thetale.app.LoginActivity;
import com.thetale.app.application.MyApplication;
import com.thetale.app.consts.StringConstants;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.http.ConnectionDetector;
import com.thetale.app.http.HttpClientFactory;
import com.thetale.app.interfaces.OnTaskCompleted;
import com.thetale.app.json.JSONParse;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rim4i4ok on 3/16/14.
 */
public class ConnectToServerTask extends AsyncTask<String, Void, Boolean> {

    private ProgressDialog dialog;

    private MyApplication application;
    private OnTaskCompleted listener;
    private SharedPreferences sharedPreferences;

    public ConnectToServerTask(LoginActivity activity, MyApplication application
                             , SharedPreferences preferences, OnTaskCompleted listener){
        dialog = new ProgressDialog(activity);
        this.application = application;
        this.sharedPreferences = preferences;
        this.listener = listener;
    }

    private Boolean isPrevUser(String login, String password) {
        Boolean result = false;

        String prevLogin = sharedPreferences.getString(StringConstants.APP_PREFERENCES_LOGIN, "");
        String prevPassword = sharedPreferences.getString(StringConstants.APP_PREFERENCES_PASSWORD, "");

        if (login.equals(prevLogin) && password.equals(prevPassword) && !prevLogin.equals("")) {
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        Boolean result = false;

        byte[] bytes = null;
        String str = "";

        // Тестовое подключение к серверу для получения sessionId и token
        HttpGet httpGet = new HttpGet(StringConstants.INIT_URL);

        BasicHttpContext mHttpContext = application.getHttpContext();
        DefaultHttpClient httpClient = application.getNewHttpClient();

        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(3);
            nameValuePairs.add(new BasicNameValuePair("email", params[0]));//"rim4i4ok@gmail.com"));
            nameValuePairs.add(new BasicNameValuePair("password", params[1]));//"UAsp7d0d"));
            nameValuePairs.add(new BasicNameValuePair("remember", "true"));

            // Запрос данных для авторизации
            HttpResponse firstResponse = httpClient.execute(httpGet, mHttpContext);
            String answer = new String(EntityUtils.toByteArray(firstResponse.getEntity()));

            // Если уже залогинилсь, то ничего не делаем
            if(!isPrevUser(params[0], params[1]) || !JSONParse.IsLogged(answer)){

                // Создание запроса на авторизацию
                HttpPost httppost = new HttpPost(StringConstants.CONNECT_URL);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                String token = application.getToken();
                httppost.addHeader(StringConstants.TOKEN, token);

                // Execute HTTP Post Request
                HttpResponse response = httpClient.execute(httppost, mHttpContext);

                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpURLConnection.HTTP_OK){
                    bytes = EntityUtils.toByteArray(response.getEntity());
                    //result = true;
                    str = new String(bytes, StringConstants.UTF_8);
                }else {
                    result = false;
                    str = statusLine.toString();
                }

                JSONParse.setConnectAnswer(str);

                result = JSONParse.IsLogIn();
            }
            else{
                result = true;
            }
        } catch (Exception exception) {
            ErrorsList.AddError(exception.toString());
        }

        // Если прошли авторизацию, то запрашиваем данные об игроке
        if(result){
            GetInfo.GetInfo(application);
        }

        return result;
    }

    @Override
    protected void onPreExecute() {
        dialog.setMessage("Подключение");
        dialog.show();
    }

    protected void onPostExecute(Boolean o){
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        listener.onTaskCompleted(o);
    }

}