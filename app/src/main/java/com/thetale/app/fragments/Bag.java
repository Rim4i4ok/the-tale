package com.thetale.app.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.adapters.BagMessageAdapter;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.json.JSONParse;
import com.thetale.app.objects.*;

import org.w3c.dom.Text;

/**
 * Created by Rim4i4ok on 4/20/14.
 */
public class Bag extends Fragment {

    private BagMessageAdapter adapter;

    public Bag(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_bag, null);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try{
            JSONParse jp = new JSONParse();
            com.thetale.app.objects.Character character = jp.getCharacter();

            // Пределы сумки
            TextView bag_count = (TextView)getActivity().findViewById(R.id.bag_count);
            bag_count.setText(character.getLootItemsCount() + "/" + character.getMaxBagSize());

            // Вещи в сумке
            this.adapter = new BagMessageAdapter(this.getActivity().getApplicationContext()
                    , R.layout.bag_row, jp.getBag());

            ListView lv = (ListView)getActivity().findViewById(R.id.list_bag);
            lv.setAdapter(this.adapter);
        }
        catch(Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

}
