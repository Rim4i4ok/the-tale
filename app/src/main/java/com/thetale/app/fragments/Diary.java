package com.thetale.app.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.thetale.app.R;
import com.thetale.app.adapters.DiaryMessageAdapter;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.json.JSONParse;
import com.thetale.app.objects.DiaryRecord;

import java.util.ArrayList;

public class Diary extends Fragment {

    private DiaryMessageAdapter adapter;
    private ArrayList<DiaryRecord> records;

    public Diary(ArrayList<DiaryRecord> rows){
        this.records = rows;
    }

    public Diary(){}

    public static Fragment newInstance(Context context) {
        return new Diary();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_diary, null);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try{
            JSONParse jp = new JSONParse();

            this.adapter = new DiaryMessageAdapter(this.getActivity().getApplicationContext()
                    , R.layout.diary_row, jp.getDiary());

            ListView lv = (ListView)getActivity().findViewById(R.id.list_diary);

            lv.setAdapter(this.adapter);
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

}
