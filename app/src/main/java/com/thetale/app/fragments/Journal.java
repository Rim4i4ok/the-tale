package com.thetale.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.thetale.app.R;
import com.thetale.app.adapters.JournalMessageAdapter;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.json.JSONParse;

public class Journal extends Fragment {

    private JournalMessageAdapter adapter;

    public Journal(){}

    public static Fragment newInstance(Context context) {
        return new Journal();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_journal, null);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try{
            JSONParse jp = new JSONParse();

            this.adapter = new JournalMessageAdapter(this.getActivity().getApplicationContext()
                    , R.layout.journal_row, jp.getJournal());

            ListView lv = (ListView)getActivity().findViewById(R.id.list_journal);
            lv.setAdapter(this.adapter);
        }
        catch(Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

}
