package com.thetale.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thetale.app.MainActivity;
import com.thetale.app.R;
import com.thetale.app.application.MyApplication;
import com.thetale.app.consts.NumberConstants;
import com.thetale.app.enums.EnumIcon;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.images.UIIcons;
import com.thetale.app.interfaces.IFragmentClickable;
import com.thetale.app.interfaces.OnTaskCompleted;
import com.thetale.app.json.JSONParse;
import com.thetale.app.objects.*;
import com.thetale.app.objects.Character;
import com.thetale.app.task.HelpHeroTask;

public class Player extends Fragment implements IFragmentClickable, OnTaskCompleted {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_player, null);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LoadData();
    }

    @Override
    public void HelpHeroClock(View view) {
        try {
            new HelpHeroTask((MainActivity)getActivity(), new MyApplication(), this).execute();
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

    @Override
    public void onTaskCompleted(Boolean value) {
        LoadData();
    }

    // Отображение данных
    private void LoadData(){
        try{
            JSONParse jp = new JSONParse();
            Character character = jp.getCharacter();

            // Имя персонажа
            setTextViewText(R.id.name, character.getName());

            // Уровень персонажа
            setTextViewText(R.id.level, Integer.toString(character.getLevel()));

            // Картинка персонажа
            ImageView player_img = (ImageView)getActivity().findViewById(R.id.player_image);
            player_img.setImageBitmap(character.getImage());

            // Уровень здоровья персонажа
            ImageView health_img = (ImageView)getActivity().findViewById(R.id.health_image);
            health_img.setImageBitmap(UIIcons.getIcon(EnumIcon.HEALTH));

            ProgressBar health_pb = (ProgressBar)getActivity().findViewById(R.id.health_pb);
            health_pb.setProgress(character.getHealth() * 100 / character.getMax_health());
            setTextViewText(R.id.health, character.getHealth() + " / " + character.getMax_health());

            // Очки опыта
            ImageView experience_img = (ImageView)getActivity().findViewById(R.id.experience_image);
            experience_img.setImageBitmap(UIIcons.getIcon(EnumIcon.EXPERIENCE));

            ProgressBar experience_pb = (ProgressBar)getActivity().findViewById(R.id.experience_pb);
            experience_pb.setProgress(character.getExperience() * 100 / character.getExperience_to_level());
            setTextViewText(R.id.experience, character.getExperience() + " / " + character.getExperience_to_level());

            // Запас энергии
            ImageView energy_img = (ImageView)getActivity().findViewById(R.id.energy_image);
            energy_img.setImageBitmap(UIIcons.getIcon(EnumIcon.ENERGY));

            ProgressBar energy_pb = (ProgressBar)getActivity().findViewById(R.id.energy_pb);
            energy_pb.setProgress(character.getEnergy_value() * 100 / character.getEnergy_max());
            setTextViewText(R.id.energy, character.getEnergy_value() + " / " + character.getEnergy_max() + " + " + character.getEnergy_bonus());

            // Физическая сила
            ImageView power_img = (ImageView)getActivity().findViewById(R.id.power_image);
            power_img.setImageBitmap(UIIcons.getIcon(EnumIcon.POWER));
            setTextViewText(R.id.power, Integer.toString(character.getPower()));

            // Магическая сила
            ImageView magic_img = (ImageView)getActivity().findViewById(R.id.magic_image);
            magic_img.setImageBitmap(UIIcons.getIcon(EnumIcon.MAGIC));
            setTextViewText(R.id.magic, Integer.toString(character.getMagic()));

            // Собранное золото
            ImageView money_img = (ImageView)getActivity().findViewById(R.id.money_image);
            money_img.setImageBitmap(UIIcons.getIcon(EnumIcon.MONEY));
            setTextViewText(R.id.money, Integer.toString(character.getMoney()));

            // Могущество
            ImageView might_img = (ImageView)getActivity().findViewById(R.id.might_image);
            might_img.setImageBitmap(UIIcons.getIcon(EnumIcon.MIGHT));
            setTextViewText(R.id.might, Double.toString(character.getMight()));

            // Честь
            setTextViewText(R.id.honor, character.getHonor_str() + " " + character.getHonor());

            // Миролюие
            setTextViewText(R.id.peacefulness, character.getPeacefulness_str() + " " + character.getPeacefulness());

            // Чем занимается
            Action action = jp.getAction();
            setTextViewText(R.id.current_action, action.getDescription());
            ProgressBar current_action_pb = (ProgressBar)getActivity().findViewById(R.id.current_action_pb);
            current_action_pb.setProgress((int)(action.getPercents() * 100));

            // Кнопка помочь
            Button button_help = (Button)getActivity().findViewById(R.id.button_help);
            if(button_help != null){
                // Если энергии не хватает, то прячем кнопку
                if((character.getEnergy_value() + character.getEnergy_bonus()) >= NumberConstants.HELP_COST){
                    button_help.setVisibility(View.VISIBLE);
                }else{
                    button_help.setVisibility(View.GONE);
                }
            }
        }
        catch(Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }


    // Установка значения контрола
    private void setTextViewText(int textViewID, String text){
        TextView tv = (TextView)getActivity().findViewById(textViewID);
        tv.setText(text);
    }
}
