package com.thetale.app.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.adapters.ErrorMessageAdapter;
import com.thetale.app.errors.ErrorsList;

public class Error extends Fragment {

    private ErrorMessageAdapter adapter;

    public Error(){}

    public static Fragment newInstance(Context context) {
        return new Error();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_error, null);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try{
            this.adapter = new ErrorMessageAdapter(this.getActivity().getApplicationContext()
                    , R.layout.error_row, ErrorsList.getErrors());

            ListView lv = (ListView)getActivity().findViewById(R.id.list_error);
            lv.setAdapter(this.adapter);
        }
        catch(Exception exception){
            String msg = exception.toString();

            TextView tv = (TextView)getActivity().findViewById(R.id.error_message);
            tv.setText(msg);
        }
    }

}
