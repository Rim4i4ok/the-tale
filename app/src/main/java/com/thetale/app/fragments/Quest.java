/**
 * Created by Rim4i4ok on 4/9/14.
 */

package com.thetale.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thetale.app.R;
import com.thetale.app.adapters.QuestMessageAdapter;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.json.JSONParse;

import android.widget.ListView;

public class Quest extends Fragment {

    private QuestMessageAdapter adapter;

    public Quest(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_quests, null);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try{
            JSONParse jp = new JSONParse();

            this.adapter = new QuestMessageAdapter(this.getActivity().getApplicationContext()
                    , R.layout.quest_row, jp.getQuests());

            ListView lv = (ListView)getActivity().findViewById(R.id.list_quests);
            lv.setAdapter(this.adapter);

        }
        catch(Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

}

