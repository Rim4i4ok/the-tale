package com.thetale.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.thetale.app.R;
import com.thetale.app.adapters.EquipmentMessageAdapter;
import com.thetale.app.adapters.JournalMessageAdapter;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.json.JSONParse;
import com.thetale.app.objects.EquipmentRecord;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 4/20/14.
 */
public class Equipment extends Fragment {

    private EquipmentMessageAdapter adapter;

    public Equipment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_eqipment, null);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try{
            JSONParse jp = new JSONParse();
            final ArrayList<EquipmentRecord> equipmentRecords = jp.getEquipment();

            this.adapter = new EquipmentMessageAdapter(this.getActivity().getApplicationContext()
                    , R.layout.equipment_row, equipmentRecords);

            ListView lv = (ListView)getActivity().findViewById(R.id.list_equipment);
            lv.setAdapter(this.adapter);

            // Пояснение к оружию
            /*lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    EquipmentRecord record = equipmentRecords.get(i);

                }
            });*/
        }
        catch(Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }
}
