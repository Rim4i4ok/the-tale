package com.thetale.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.thetale.app.R;

import com.thetale.app.errors.ErrorsList;
import com.thetale.app.json.JSONParse;

public class Connect extends Fragment {

    public Connect(){}

    public static Fragment newInstance(Context context) {
        return new Connect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_connect, null);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try{
            TextView tv = (TextView)getActivity().findViewById(R.id.connectInfo);
            tv.setText(JSONParse.getConnectAnswer());
        }
        catch(Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

}
