package com.thetale.app.json;

import com.thetale.app.errors.ErrorsList;
import com.thetale.app.objects.*;
import com.thetale.app.objects.Character;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Created by Rim4i4ok on 3/22/14.
 */
public class JSONParse {

    // Данные для разбора
    private JSONData _data = JSONData.getInstance();

    public JSONParse(){}

    // JSON из ответа при подключении
    private static String _connectAnswer;
    public static void setConnectAnswer(String connectAnswer){
        _connectAnswer = connectAnswer;
    }
    public static String getConnectAnswer() { return _connectAnswer; }

    // JSON из запроса об информации об игре/герое
    //private static String _info;
    //public static void setInfo(String json){
    //    _info = json;
    //}

    private static final String TAG_DATA = "data";
    private static final String TAG_ACCOUNT = "account";
    private static final String TAG_HERO = "hero";
    private static final String TAG_DIARY = "diary";
    private static final String TAG_MESSAGE = "messages";
    private static final String TAG_BASE = "base";
    private static final String TAG_NAME = "name";
    private static final String TAG_RACE = "race";
    private static final String TAG_MONEY = "money";
    private static final String TAG_MAX_HEALTH = "max_health";
    private static final String TAG_HEALTH = "health";
    private static final String TAG_LEVEL = "level";
    private static final String TAG_GENDER = "gender";
    private static final String TAG_EXPERIENCE = "experience";
    private static final String TAG_EXPERIENCE_TO_LEVEL = "experience_to_level";
    private static final String TAG_ENERGY = "energy";
    private static final String TAG_BONUS = "bonus";
    private static final String TAG_MAX = "max";
    private static final String TAG_VALUE = "value";
    private static final String TAG_ACTION = "action";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_PERCENTS = "percents";
    private static final String TAG_INFO_LINK = "info_link";
    private static final String TAG_TYPE = "type";
    private static final String TAG_QUESTS = "quests";
    private static final String TAG_LINE = "line";
    private static final String TAG_UID = "uid";
    private static final String TAG_CHOICE = "choice";
    private static final String TAG_POWER = "power";
    private static final String TAG_ACTORS = "actors";
    private static final String TAG_ID = "id";
    private static final String TAG_PLACE = "place";
    private static final String TAG_PROFESSION = "profession";
    private static final String TAG_MASTERY_VERBOSE = "mastery_verbose";
    private static final String TAG_EQUIPMENT = "equipment";
    private static final String TAG_EQUIPPED = "equipped";
    private static final String TAG_GOAL = "goal";
    private static final String TAG_BAG = "bag";
    private static final String TAG_SECONDARY = "secondary";
    private static final String TAG_MIGHT = "might";
    private static final String TAG_HABITS = "habits";
    private static final String TAG_PEACEFULNESS = "peacefulness";
    private static final String TAG_HONOR = "honor";
    private static final String TAG_RAW = "raw";
    private static final String TAG_VERBOSE = "verbose";
    private static final String TAG_LOOT_ITEMS_COUNT = "loot_items_count";
    private static final String TAG_MAX_BAG_SIZE = "max_bag_size";
    private static final String TAG_CHOICE_ALTERNATIVES = "choice_alternatives";
    private static final String TAG_RARITY = "rarity";
    private static final String TAG_EFFECT = "effect";
    private static final String TAG_INTEGRITY = "integrity";

    // Получение записей из дневника персонажа
    public ArrayList<DiaryRecord> getDiary(){

        ArrayList<DiaryRecord> result = new ArrayList<DiaryRecord>();

        try{
            JSONObject jsonObject = new JSONObject(_data.getData());
            JSONArray diaryArray = jsonObject.getJSONObject(TAG_DATA)
                                             .getJSONObject(TAG_ACCOUNT)
                                             .getJSONObject(TAG_HERO)
                                             .getJSONArray(TAG_DIARY);

            for(int i = 0; i < diaryArray.length(); i++){
                JSONArray values = diaryArray.getJSONArray(i);

                String time = values.getString(1);
                String message = values.getString(2);
                String date = values.getString(3);

                result.add(new DiaryRecord(message, time, date));
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        Collections.reverse(result);

        return result;
    }

    // Экипировка героя
    public ArrayList<EquipmentRecord> getEquipment(){

        ArrayList<EquipmentRecord> result = new ArrayList<EquipmentRecord>();

        try{
            JSONObject jsonObject = new JSONObject(_data.getData());
            JSONObject equipmentObject = jsonObject.getJSONObject(TAG_DATA)
                                                   .getJSONObject(TAG_ACCOUNT)
                                                   .getJSONObject(TAG_HERO)
                                                   .getJSONObject(TAG_EQUIPMENT);

            Iterator<String> iter = equipmentObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                JSONObject value = equipmentObject.getJSONObject(key);

                EquipmentRecord equipment = new EquipmentRecord();

                JSONArray power = value.getJSONArray(TAG_POWER);
                equipment.setPower(power.getInt(0));
                equipment.setMagic(power.getInt(1));

                // Тип экипировки
                equipment.setType(value.getInt(TAG_TYPE));

                // Название
                equipment.setName(value.getString(TAG_NAME));

                // Редкость
                equipment.setRarity(value.getInt(TAG_RARITY));

                // Эффект
                equipment.setEffect(value.getInt(TAG_EFFECT));

                // Целостность
                JSONArray integrity = value.getJSONArray(TAG_INTEGRITY);
                equipment.setIntegrity(integrity.getInt(0) / integrity.getInt(1) * 100);

                result.add(equipment);
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        Collections.sort(result, new Comparator<EquipmentRecord>() {
            @Override
            public int compare(EquipmentRecord equipmentRecord, EquipmentRecord equipmentRecord2) {

                Integer type = equipmentRecord.getType();
                Integer typ2 = equipmentRecord2.getType();

                return type.compareTo(typ2);
            }
        });

        return result;
    }

    // Список предметов в сумке
    public ArrayList<BagRecord> getBag(){

        ArrayList<BagRecord> result = new ArrayList<BagRecord>();

        try{
            JSONObject jsonObject = new JSONObject(_data.getData());
            JSONObject bagObject = jsonObject.getJSONObject(TAG_DATA)
                                             .getJSONObject(TAG_ACCOUNT)
                                             .getJSONObject(TAG_HERO)
                                             .getJSONObject(TAG_BAG);

            Iterator<String> iter = bagObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                JSONObject value = bagObject.getJSONObject(key);

                BagRecord bag = new BagRecord();

                bag.setType(value.getInt(TAG_TYPE));
                bag.setId(value.getInt(TAG_ID));
                bag.setName(value.getString(TAG_NAME));

                result.add(bag);
            }

        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return result;

    }

    // Получение записей из журнала событий
    public ArrayList<JournalRecord> getJournal(){

        ArrayList<JournalRecord> result = new ArrayList<JournalRecord>();

        try{
            JSONObject jsonObject = new JSONObject(_data.getData());
            JSONArray messageArray = jsonObject.getJSONObject(TAG_DATA)
                                               .getJSONObject(TAG_ACCOUNT)
                                               .getJSONObject(TAG_HERO)
                                               .getJSONArray(TAG_MESSAGE);

            for(int i = 0; i < messageArray.length(); i++){
                JSONArray values = messageArray.getJSONArray(i);

                String date = values.getString(1);
                String message = values.getString(2);

                result.add(new JournalRecord(date, message));
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        Collections.reverse(result);
        return result;
    }

    // Получение информации о персонаже
    public Character getCharacter(){

        Character result = new Character();

        try{
            JSONObject jsonObject = new JSONObject(_data.getData());

            // base
            JSONObject character = jsonObject.getJSONObject(TAG_DATA)
                                             .getJSONObject(TAG_ACCOUNT)
                                             .getJSONObject(TAG_HERO)
                                             .getJSONObject(TAG_BASE);

            result.setName(character.getString(TAG_NAME));
            result.setMoney(character.getInt(TAG_MONEY));
            result.setMax_health(character.getInt(TAG_MAX_HEALTH));
            result.setHealth(character.getInt(TAG_HEALTH));
            result.setGender(character.getInt(TAG_GENDER));
            result.setRace(character.getInt(TAG_RACE));
            result.setLevel(character.getInt(TAG_LEVEL));
            result.setExperience(character.getInt(TAG_EXPERIENCE));
            result.setExperience_to_level(character.getInt(TAG_EXPERIENCE_TO_LEVEL));

            // energy
            JSONObject energy = jsonObject.getJSONObject(TAG_DATA)
                                          .getJSONObject(TAG_ACCOUNT)
                                          .getJSONObject(TAG_HERO)
                                          .getJSONObject(TAG_ENERGY);

            result.setEnergy_bonus(energy.getInt(TAG_BONUS));
            result.setEnergy_max(energy.getInt(TAG_MAX));
            result.setEnergy_value(energy.getInt(TAG_VALUE));

            // power
            JSONObject secondary = jsonObject.getJSONObject(TAG_DATA)
                                             .getJSONObject(TAG_ACCOUNT)
                                             .getJSONObject(TAG_HERO)
                                             .getJSONObject(TAG_SECONDARY);

            JSONArray power = secondary.getJSONArray(TAG_POWER);
            result.setPower(power.getInt(0));
            result.setMagic(power.getInt(1));

            // Bag
            result.setLootItemsCount(secondary.getInt(TAG_LOOT_ITEMS_COUNT));
            result.setMaxBagSize(secondary.getInt(TAG_MAX_BAG_SIZE));

            // might
            JSONObject might = jsonObject.getJSONObject(TAG_DATA)
                                         .getJSONObject(TAG_ACCOUNT)
                                         .getJSONObject(TAG_HERO)
                                         .getJSONObject(TAG_MIGHT);

            result.setMight(might.getDouble(TAG_VALUE));

            // habits
            JSONObject habits = jsonObject.getJSONObject(TAG_DATA)
                                          .getJSONObject(TAG_ACCOUNT)
                                          .getJSONObject(TAG_HERO)
                                          .getJSONObject(TAG_HABITS);

            JSONObject honor = habits.getJSONObject(TAG_HONOR);
            result.setHonor(honor.getDouble(TAG_RAW));
            result.setHonor_str(honor.getString(TAG_VERBOSE));

            JSONObject peacefulness = habits.getJSONObject(TAG_PEACEFULNESS);
            result.setPeacefulness(peacefulness.getDouble(TAG_RAW));
            result.setPeacefulness_str(peacefulness.getString(TAG_VERBOSE));

        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return result;
    }

    // Что происходит в текущий момент
    public Action getAction(){

        Action result = new Action();

        try{
            JSONObject jsonObject = new JSONObject(_data.getData());

            JSONObject values = jsonObject.getJSONObject(TAG_DATA)
                                          .getJSONObject(TAG_ACCOUNT)
                                          .getJSONObject(TAG_HERO)
                                          .getJSONObject(TAG_ACTION);


            result.setDescription(values.getString(TAG_DESCRIPTION));
            result.setPercents(values.getDouble(TAG_PERCENTS));
            result.setInfo_link(values.getString(TAG_INFO_LINK));
            result.setType(values.getInt(TAG_TYPE));
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return result;

    }

    // Получить сюжетный квест
    public ArrayList<QuestRecord> getQuests(){
        ArrayList<QuestRecord> result = new ArrayList<QuestRecord>();

        try{
            JSONObject jsonObject = new JSONObject(_data.getData());

            JSONArray quests = jsonObject.getJSONObject(TAG_DATA)
                                             .getJSONObject(TAG_ACCOUNT)
                                             .getJSONObject(TAG_HERO)
                                             .getJSONObject(TAG_QUESTS)
                                             .getJSONArray(TAG_QUESTS);

            int questCount = quests.length();

            for(int i = 0; i < questCount; i++) {
                JSONArray values = quests.getJSONObject(i).getJSONArray(TAG_LINE);
                result.addAll(getQuests(values));
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return result;
    }

    // Обычный квест
    private static ArrayList<QuestRecord> getQuests(JSONArray array){

        ArrayList<QuestRecord> result = new ArrayList<QuestRecord>();

        try{
            for(int i = 0; i < array.length(); i++ ) {
                QuestRecord record = new QuestRecord();

                JSONObject quest = array.getJSONObject(i);
                // тип квеста
                record.setUid(quest.getString(TAG_UID));
                // влияние квеста
                record.setPower(quest.getInt(TAG_POWER));
                // название квеста
                record.setName(quest.getString(TAG_NAME));
                // описание текущего действия
                record.setAction(quest.getString(TAG_ACTION));
                // текущий выбор в задании
                record.setChoice(quest.getString(TAG_CHOICE));
                // список участников
                JSONArray actors = quest.getJSONArray(TAG_ACTORS);
                for(int j = 0; j < actors.length(); j++) {
                    Actor actor = new Actor();

                    JSONArray currentActor = actors.getJSONArray(j);
                    // название участника
                    actor.setActorName(currentActor.getString(0));
                    // тип участника
                    actor.setType(currentActor.getInt(1));

                    JSONObject currentActorDescription = currentActor.getJSONObject(2);

                    if(!currentActorDescription.isNull(TAG_GOAL)){
                        // тип квеста
                        record.setQuest_type(0);

                        // цель
                        actor.setGoal(currentActorDescription.getString(TAG_GOAL));

                        // описание
                        actor.setDescription(currentActorDescription.getString(TAG_DESCRIPTION));
                    }
                    else {
                        // тип квеста
                        record.setQuest_type(0);

                        // мастерство
                        if(!currentActorDescription.isNull(TAG_MASTERY_VERBOSE)) {
                            actor.setMastery_verbose(currentActorDescription.getString(TAG_MASTERY_VERBOSE));
                        }
                        // имя участника
                        if(!currentActorDescription.isNull(TAG_NAME)) {
                            actor.setName(currentActorDescription.getString(TAG_NAME));
                        }
                        // пол
                        if(!currentActorDescription.isNull(TAG_GENDER)) {
                            actor.setGender(currentActorDescription.getInt(TAG_GENDER));
                        }
                        // профессия
                        if(!currentActorDescription.isNull(TAG_PROFESSION)) {
                            actor.setProfession(currentActorDescription.getInt(TAG_PROFESSION));
                        }
                        // раса
                        if(!currentActorDescription.isNull(TAG_RACE)) {
                            actor.setRace(currentActorDescription.getInt(TAG_RACE));
                        }
                        // идентификатор города
                        if(!currentActorDescription.isNull(TAG_PLACE)) {
                            actor.setPlace(currentActorDescription.getInt(TAG_PLACE));
                        }
                        // идентификатор участника
                        if(!currentActorDescription.isNull(TAG_ID)) {
                            actor.setId(currentActorDescription.getInt(TAG_ID));
                        }
                    }

                    record.addActor(actor);
                }
                // тип квеста
                record.setType(quest.getString(TAG_TYPE));
                // опыт за задание
                record.setExperience(quest.getInt(TAG_EXPERIENCE));
                // альтенативные выборы в задании
                JSONArray choices = quest.getJSONArray(TAG_CHOICE_ALTERNATIVES);
                for(int k = 0; k < choices.length(); k++){
                    Choice choice = new Choice();

                    JSONArray currentChoice = choices.getJSONArray(k);

                    // идентификатор выбора
                    choice.setUid(currentChoice.getString(0));
                    // описание выбора
                    choice.setDescription(currentChoice.getString(1));

                    record.addChoice(choice);
                }

                result.add(record);
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return  result;
    }

    // Сюжетный квест
    private static QuestRecord getStoryQuest(JSONArray array){

        QuestRecord result = new QuestRecord();

        try{
            JSONObject quest = array.getJSONObject(0);

            // тип квеста
            result.setUid(quest.getString(TAG_UID));
            // влияние квеста
            result.setPower(quest.getInt(TAG_POWER));
            // название квеста
            result.setName(quest.getString(TAG_NAME));
            // описание текущего действия
            result.setAction(quest.getString(TAG_ACTION));
            // текущий выбор в задании
            result.setChoice(quest.getString(TAG_CHOICE));
            // список участников
            JSONArray actors = quest.getJSONArray(TAG_ACTORS);
            for(int i = 0; i < actors.length(); i++) {
                Actor actor = new Actor();

                JSONArray currentActor = actors.getJSONArray(i);
                // название участника
                actor.setActorName(currentActor.getString(0));
                // тип участника
                actor.setType(currentActor.getInt(1));

                JSONObject currentActorDescription = currentActor.getJSONObject(2);
                // цель
                actor.setGoal(currentActorDescription.getString(TAG_GOAL));
                // описание
                actor.setDescription(currentActorDescription.getString(TAG_DESCRIPTION));

                result.addActor(actor);
            }
            // тип квеста
            result.setType(quest.getString(TAG_TYPE));
            // опыт за задание
            result.setExperience(quest.getInt(TAG_EXPERIENCE));

            // тип квеста
            result.setQuest_type(0);

        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return result;

    }

    // Пройдена ли авторизация на сервер
    public static Boolean IsLogIn(){

        try{
            JSONObject jsonObject = new JSONObject(_connectAnswer);
            String status = jsonObject.getString("status");

            if(status.equalsIgnoreCase("ok")){
                return true;
            }else{
                return false;
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return false;
    }

    // Проверка, что мы не авторизованы
    public static Boolean IsLogged(String answer){

        Boolean result = false;

        try{
            JSONObject jsonObject = new JSONObject(answer);

            String account = jsonObject.getJSONObject(TAG_DATA)
                                       .getString(TAG_ACCOUNT);

            if(account != "null"){
                result = true;
            }
            else{
                result = false;
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        return result;
    }
}
