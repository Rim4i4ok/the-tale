package com.thetale.app.json;

import java.util.Observable;

/**
 * Created by Rim4i4ok on 13.06.2014.
 */
public class JSONData extends Observable {

    // Singleton
    private JSONData(){ this.data = ""; }
    private static final JSONData INSTANCE = new JSONData();
    public static JSONData getInstance(){ return INSTANCE; }

    // Строка с данными в JSON формате
    private String data;

    // Установка данных
    public void setData(String data){
        this.data = data;

        triggerObservers();
    }

    // получение данных
    public String getData(){
        return this.data;
    }

    // Если данные поменялись, то предупреждаем всех
    private void triggerObservers(){
        setChanged();
        notifyObservers();
    }

}
