package com.thetale.app.objects;

/**
 * Created by Rim4i4ok on 4/20/14.
 */
public class BagRecord {

    private Boolean equipped;
    public void setEquipped(Boolean equipped) {
        this.equipped = equipped;
    }

    private int type;
    public void setType(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }

    private int id;
    public void setId(int id) {
        this.id = id;
    }

    private int power;
    public void setPower(int power) {
        this.power = power;
    }

    private String name;
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
