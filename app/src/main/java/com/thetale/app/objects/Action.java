package com.thetale.app.objects;

/**
 * Created by Rim4i4ok on 4/1/14.
 */
public class Action {

    private String description;

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    private double percents;

    public void setPercents(double percents) {
        this.percents = percents;
    }

    public double getPercents() {
        return percents;
    }

    private String info_link;

    public void setInfo_link(String info_link) {
        this.info_link = info_link;
    }

    private int type;

    public void setType(int type) {
        this.type = type;
    }
}
