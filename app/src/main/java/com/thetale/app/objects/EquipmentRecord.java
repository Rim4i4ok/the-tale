package com.thetale.app.objects;

import android.graphics.Bitmap;
import android.os.Parcelable;

/**
 * Created by Rim4i4ok on 4/19/14.
 */
public class EquipmentRecord {

    public EquipmentRecord(){}

    private int power;
    public void setPower(int power) { this.power = power; }
    public int getPower(){ return this.power; }

    private int magic;
    public void setMagic(int magic) { this.magic = magic; }
    public int getMagic() { return magic; }

    private String name;
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    private int type;
    public void setType(int type) {
        this.type = type;
    }
    public int getType() {
        return type;
    }

    private int rarity;
    public void setRarity(int rarity) { this.rarity = rarity; }
    public int getRarity() { return rarity; }

    private int effect;
    public void setEffect(int effect) { this.effect = effect; }
    public int getEffect() { return effect; }

    private double integrity;
    public void setIntegrity(double integrity) { this.integrity = integrity; }
    public double getIntegrity() { return integrity; }
}
