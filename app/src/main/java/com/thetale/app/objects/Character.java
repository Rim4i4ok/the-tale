package com.thetale.app.objects;

import android.graphics.Bitmap;

import com.thetale.app.enums.EnumRace;
import com.thetale.app.enums.EnumSex;
import com.thetale.app.images.UIMap;

import java.util.ArrayList;

/**
 * Created by Rim4i4ok on 3/30/14.
 */
public class Character {

    private String name;
    public void setName(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }

    private int money;
    public void setMoney(int money){
        this.money = money;
    }
    public int getMoney() { return money; }

    private int max_health;
    public void setMax_health(int max_health){
        this.max_health = max_health;
    }
    public int getMax_health() {
        return max_health;
    }

    private int health;
    public void setHealth(int health){
        this.health = health;
    }
    public int getHealth() {
        return health;
    }

    private int level;
    public void setLevel(int level){
        this.level = level;
    }
    public int getLevel() {
        return level;
    }

    private int experience;
    public void setExperience(int experience) {
        this.experience = experience;
    }
    public int getExperience() {
        return experience;
    }

    private int experience_to_level;
    public void setExperience_to_level(int experience_to_level) { this.experience_to_level = experience_to_level; }
    public int getExperience_to_level() {
        return experience_to_level;
    }

    private int energy_bonus;
    public void setEnergy_bonus(int energy_bonus) {
        this.energy_bonus = energy_bonus;
    }
    public int getEnergy_bonus() {
        return energy_bonus;
    }

    private int energy_max;
    public void setEnergy_max(int energy_max) {
        this.energy_max = energy_max;
    }
    public int getEnergy_max() {
        return energy_max;
    }

    private int energy_value;
    public void setEnergy_value(int energy_value) {
        this.energy_value = energy_value;
    }
    public int getEnergy_value() {
        return energy_value;
    }

    // Физическая сила
    private int power;
    public void setPower(int power) { this.power = power; }
    public int getPower() { return power; }

    // Магическая сила
    private int magic;
    public void setMagic(int magic) { this.magic = magic; }
    public int getMagic() { return magic; }

    private double might;
    public void setMight(double might) { this.might = might; }
    public double getMight() { return might; }

    // черты
    // честь
    private double honor;
    public void setHonor(double honor) { this.honor = honor; }
    public double getHonor() { return honor; }

    private String honor_str;
    public void setHonor_str(String honor_str) { this.honor_str = honor_str; }
    public String getHonor_str() { return honor_str; }

    // миролюбие
    private double peacefulness;
    public void setPeacefulness(double peacefulness) { this.peacefulness = peacefulness; }
    public double getPeacefulness() { return peacefulness; }

    private String peacefulness_str;
    public void setPeacefulness_str(String peacefulness_str) { this.peacefulness_str = peacefulness_str; }
    public String getPeacefulness_str() { return peacefulness_str; }

    // Сумка
    // Количество вещей в сумке
    private int lootItemsCount;
    public void setLootItemsCount(int lootItemsCount) { this.lootItemsCount = lootItemsCount; }
    public int getLootItemsCount() { return lootItemsCount; }

    // Объем сумки
    private int maxBagSize;
    public void setMaxBagSize(int maxBagSize) { this.maxBagSize = maxBagSize; }
    public int getMaxBagSize() { return maxBagSize; }

    // Пол
    private int gender;
    public void setGender(int gender) { this.gender = gender; }
    public int getGender() { return gender; }

    // Раса
    private int race;
    public void setRace(int race) { this.race = race;  }
    public int getRace() { return race;  }

    // Картинка персонажа
    public Bitmap getImage(){

        int image_start_id = 96;
        int image_gender = 0;

        switch (this.gender){
            case 0:
                image_gender = EnumSex.MALE;
                break;
            case 1:
                image_gender = EnumSex.FEMALE;
                break;
        }

        switch (this.race){
            // Человек
            case EnumRace.HUMAN:
                image_start_id = 96;
                break;
            // Эльф
            case EnumRace.ELF:
                image_start_id = 100;
                break;
            // Орк
            case EnumRace.ORC:
                image_start_id = 104;
                break;
            // Гоблин
            case EnumRace.GOBLIN:
                image_start_id = 102;
                break;
            // Дфарф
            case EnumRace.DWARF:
                image_start_id = 98;
                break;
        }

        return UIMap.getIcon(image_start_id + image_gender);

    }

    public Character(){}
}
