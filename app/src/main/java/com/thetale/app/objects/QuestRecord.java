package com.thetale.app.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rim4i4ok on 4/1/14.
 */
public class QuestRecord {

    // --------------------My properties----------------------------------------

    // Тип квеста
    // 0 - сюжетный
    // 1- взятый
    private int quest_type;
    public void setQuest_type(int quest_type) { this.quest_type = quest_type; }
    public int getQuest_type() { return quest_type; }

    // --------------------My properties----------------------------------------

    private String uid;
    public void setUid(String uid) {
        this.uid = uid;
    }

    private int power;
    public void setPower(int power) { this.power = power; }
    public int getPower() { return power; }

    private String name;
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    private int experience;
    public void setExperience(int experience) {
        this.experience = experience;
    }
    public int getExperience() { return experience;  }

    private String choice;
    public void setChoice(String choice) {
        this.choice = choice;
    }
    public String getChoice() { return choice; }

    private ArrayList<Actor> actors = new ArrayList<Actor>();
    public void addActor(Actor actor){
        this.actors.add(actor);
    }
    public ArrayList<Actor> getActors() { return actors; }

    private String action;
    public void setAction(String action) {
        this.action = action;
    }
    public String getAction() { return action; }

    private String type;
    public void setType(String type) {
        this.type = type;
    }
    public String getType() { return type; }

    private List<Choice> choice_alternatives = new ArrayList<Choice>();
    public void addChoice(Choice choice) { this.choice_alternatives.add(choice); }
    public List<Choice> getChoice_alternatives() { return choice_alternatives; }
}
