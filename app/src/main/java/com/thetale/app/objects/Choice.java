package com.thetale.app.objects;

/**
 * Created by Rim4i4ok on 22.05.2014.
 */
public class Choice {

    // Уникальный идентификатор выбора
    private String uid;
    public void setUid(String uid) { this.uid = uid; }
    public String getUid() { return uid; }

    // Описание выбора
    private String description;
    public void setDescription(String description) { this.description = description; }
    public String getDescription() { return description; }
}
