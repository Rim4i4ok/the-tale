package com.thetale.app.objects;

/**
 * Created by Rim4i4ok on 3/30/14.
 */
public class JournalRecord {

    private String date;
    private String message;

    public JournalRecord(String date, String message){
        this.date = date;
        this.message = message;
    }

    public String getDate() { return date; }
    public String getMessage() { return message; }

}
