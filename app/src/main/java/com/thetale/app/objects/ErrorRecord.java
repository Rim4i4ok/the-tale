package com.thetale.app.objects;

import java.util.Date;

/**
 * Created by Rim4i4ok on 3/30/14.
 */
public class ErrorRecord {

    private Date _date;
    private String _message;

    public Date getDate() { return _date; }

    public void setMessage(String message){ _message = message; }
    public String getMessage() { return _message; }

    public ErrorRecord(String message){
        this._date = new Date();
        this._message = message;
    }

}
