package com.thetale.app.objects;

/**
 * Created by Rim4i4ok on 4/1/14.
 */
public class Actor {

    private String actorName;
    public void setActorName(String actorName) { this.actorName = actorName;  }
    public String getActorName() { return actorName; }

    private int type;
    public void setType(int type) { this.type = type;  }

    // ---------------------------Story Quest----------------------------------------

    private String goal;
    public void setGoal(String goal) { this.goal = goal; }
    public String getGoal() {  return goal; }

    private String description;
    public void setDescription(String description) { this.description = description; }
    public String getDescription() { return description; }

    // ------------------------------------------------------------------------------

    private String mastery_verbose;
    public void setMastery_verbose(String mastery_verbose) {
        this.mastery_verbose = mastery_verbose;
    }

    private String name;
    public void setName(String name) {
        this.name = name;
    }
    public String getName() { return name; }

    private int gender;
    public void setGender(int gender) {
        this.gender = gender;
    }

    private int profession;
    public void setProfession(int profession) {
        this.profession = profession;
    }

    private int race;
    public void setRace(int race) {
        this.race = race;
    }

    private int place;
    public void setPlace(int place) {
        this.place = place;
    }

    private int id;
    public void setId(int id) {
        this.id = id;
    }
}
