package com.thetale.app.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Rim4i4ok on 3/23/14.
 */
public class DiaryRecord implements Parcelable {

    private String message;
    private String time;
    private String date;

    public DiaryRecord(String message, String time, String date){
        this.message = message;
        this.time = time;
        this.date = date;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }

    public void setTime(String time){
        this.time = time;
    }

    public String getTime(){
        return this.time;
    }

    public void setDate(String date){
        this.date = date;
    }

    public String getDate(){
        return this.date;
    }

    // Parcelable implementation
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags){
        out.writeString(message);
        out.writeString(time);
        out.writeString(date);
    }

    public static final Parcelable.Creator<DiaryRecord> CREATOR
            = new Parcelable.Creator<DiaryRecord>() {
        public DiaryRecord createFromParcel(Parcel in) {
            return new DiaryRecord(in);
        }

        public DiaryRecord[] newArray(int size) {
            return new DiaryRecord[size];
        }
    };

    private DiaryRecord(Parcel in){
        message = in.readString();
        time = in.readString();
        date = in.readString();
    }
    // Parcelable implementation end


}
