package com.thetale.app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.thetale.app.adapters.TabsPagerAdapter;
import com.thetale.app.json.JSONData;
import com.thetale.app.application.MyApplication;
import com.thetale.app.enums.EnumFragment;
import com.thetale.app.errors.ErrorsList;
import com.thetale.app.fragments.Player;
import com.thetale.app.http.ConnectionDetector;
import com.thetale.app.images.UIIcons;
import com.thetale.app.images.UIMap;
import com.thetale.app.images.UIQuests;
import com.thetale.app.interfaces.OnTaskCompleted;
import com.thetale.app.task.GetInfo;
import com.thetale.app.task.GetInfoThread;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends FragmentActivity implements OnTaskCompleted, Observer, ActionBar.TabListener {

    private ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    private JSONData _data;

    private MenuItem refreshMenuItem;

    MyApplication application;
    Context _context;
    ConnectionDetector _detector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        application = new MyApplication();
        _context = getApplicationContext();
        _detector = new ConnectionDetector(_context);
        _data = JSONData.getInstance();

        _data.addObserver(this);

        new Thread(new GetInfoThread(application, _context)).start();

        // Подгрузка спрайтов
        try{
            // Картинки для персонажа и экипировки
            UIIcons.setImagesCount(19);
            UIIcons.setIcons(BitmapFactory.decodeResource(_context.getResources(), R.drawable.game_ui));

            // Картинки для квестов
            UIQuests.setColumnCount(2);
            UIQuests.setRowCount(12);
            UIQuests.setIcons(BitmapFactory.decodeResource(_context.getResources(), R.drawable.quests));

            // Картинки для карты и расы
            UIMap.setColumnCount(12);
            UIMap.setRowCount(10);
            UIMap.setIcons(BitmapFactory.decodeResource(_context.getResources(), R.drawable.map));
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }

        try{
            InitTabAdapter();

            // Tabs
            for(String tab_name: EnumFragment.getData()){
                this.actionBar.addTab(this.actionBar.newTab()
                        .setText(tab_name)
                        .setTabListener(this));
            }

            this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    actionBar.setSelectedNavigationItem(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

    // Когда данные изменились, то обновляем данные на экране
    @Override
    public void update(Observable observable, Object data) {
        try {
            new Thread() {
                public void run() {
                    tabsHandler.sendEmptyMessage(0);
                }
            }.start();
        }
        catch(Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

    // Handler для перерисовки данных
    private Handler tabsHandler = new Handler(){
        public void handleMessage(Message msg){
            InitTabAdapter();
        }
    };

    // Настройка отрисовки вкладочек
    private void InitTabAdapter(){
        // Init
        this.viewPager = (ViewPager)findViewById(R.id.pager);
        this.actionBar = getActionBar();
        this.mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        this.viewPager.setAdapter(this.mAdapter);

        this.actionBar.setHomeButtonEnabled(false);
        this.actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {}

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {}

    // Клик кнопки помощи
    public void HelpHero(View v){
        try {
            if (_detector.isConnectingToInternet()) {
                android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                Player f = (Player) fm.getFragments().get(0);
                f.HelpHeroClock(v);
            } else {
                showInternetAlert();
            }
        }
        catch (Exception exception){
            ErrorsList.AddError(exception.toString());
        }
    }

    // Окошко с предупреждением об отсутствии интернета
    private void showInternetAlert(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Интернет");

        // Setting Dialog Message
        alertDialog.setMessage("Отсутствует подключение к интернету");

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
            }
        });


        alertDialog.setIcon(R.drawable.game_icon);
        // Showing Alert Message
        alertDialog.show();
    }

    // Авторизация окончена
    public void onTaskCompleted(Boolean value){
        if(value){

            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
    }

    // Обновить кнопка
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_refresh:
                refreshMenuItem = item;
                // load the data from server
                new RefreshData().execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Кнопочка обновить
    private class RefreshData extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            refreshMenuItem.setActionView(R.layout.action_progressbar);
            refreshMenuItem.expandActionView();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if(_detector.isConnectingToInternet()) {
                    GetInfo.GetInfo(application);
                }
            } catch (Exception exception) {
                ErrorsList.AddError(exception.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            refreshMenuItem.collapseActionView();
            refreshMenuItem.setActionView(null);
        }
    }

}
