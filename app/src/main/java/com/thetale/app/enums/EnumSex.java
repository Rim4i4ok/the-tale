package com.thetale.app.enums;

/**
 * Created by Rim4i4ok on 3/31/14.
 */
public class EnumSex {

    private static final int START = 0;

    public static final int MALE = 0;
    public static final int FEMALE = 1;
    public static final int IT = 2;

    private static final int END = 2;

    private String[] description = { "Мужчина"
                                   , "Женщина"
                                   , "Оно" };

    public String getSex(int sexId){
        if(sexId >= START && sexId <= END){
            return description[sexId];
        }
        else{
            return "Неизвестно";
        }
    }

}
