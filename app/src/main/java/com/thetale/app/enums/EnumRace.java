package com.thetale.app.enums;

/**
 * Created by Rim4i4ok on 3/31/14.
 */
public class EnumRace {

    private static final int START = 0;

    public static final int HUMAN = 0;
    public static final int ELF = 1;
    public static final int ORC = 2;
    public static final int GOBLIN = 3;
    public static final int DWARF = 4;

    private static final int END = 4;

    private String[] description = { "Человек"
                                   , "Эльф"
                                   , "Орк"
                                   , "Гоблин"
                                   , "Дварф" };

    public String getRace(int raceId){
        if(raceId >= START && raceId <= END){
            return description[raceId];
        }
        else{
            return "Неизвестно";
        }
    }

}
