package com.thetale.app.enums;

/**
 * Created by Rim4i4ok on 24.05.2014.
 */
public class EnumFragment {

    private final int CONNECT_INFO = -2;
    private final int ERRORS = -1;

    public static final int PLAYER = 0;
    public static final int EQUIPMENT = 1;
    public static final int QUESTS = 2;
    public static final int BAG = 3;
    public static final int DIARY = 4;
    public static final int JOURNAL = 5;

    private static String[] data = { //"Информация подключения"
                                //, "Ошибки"
                                  "Персонаж"
                                , "Экипировка"
                                , "Задания"
                                , "Рюкзак"
                                , "Дневник"
                                , "Журнал"};

    public static String[] getData() { return data; }
    public static int getCount() { return data.length; }
}
