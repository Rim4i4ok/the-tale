package com.thetale.app.enums;

/**
 * Created by Rim4i4ok on 4/12/14.
 */
public class EnumIcon {

    public static final int HEALTH = 0;
    public static final int EXPERIENCE = 1;
    public static final int ENERGY = 2;
    public static final int POWER = 3;
    public static final int MAGIC = 4;
    public static final int MONEY = 5;
    public static final int MIGHT = 18;

}
