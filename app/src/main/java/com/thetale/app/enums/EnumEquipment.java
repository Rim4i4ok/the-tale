package com.thetale.app.enums;

import android.graphics.Bitmap;

import com.thetale.app.images.UIIcons;

/**
 * Created by Rim4i4ok on 3/31/14.
 */
public class EnumEquipment {

    private static final int START = 0;

    public static final int TRASH = 0;
    public static final int MAIN_HAND = 1;
    public static final int SECOND_HAND = 2;
    public static final int ARMOR = 3;
    public static final int AMULET = 4;
    public static final int HELMET = 5;
    public static final int CLOAK = 6;
    public static final int SHOULDERS = 7;
    public static final int GLOVES = 8;
    public static final int PANTS = 9;
    public static final int SHOES = 10;
    public static final int RING = 11;

    private static final int END = 11;

    private static String[] description = { "Хлам"
                                   , "Основная рука"
                                   , "Вторая рука"
                                   , "Доспех"
                                   , "Амулет"
                                   , "Шлем"
                                   , "Плащ"
                                   , "Наплечники"
                                   , "Перчатки"
                                   , "Штаны"
                                   , "Обувь"
                                   , "Кольцо"  };

    public static String getArtifact(int artId){
        if(artId >= START && artId <= END){
            return description[artId];
        }
        else{
            return "Неизвестно";
        }
    }

    // Получение картинки экипировки
    public static Bitmap getImage(int type){

        switch (type){
            case MAIN_HAND:
                return UIIcons.getIcon(6);

            case SECOND_HAND:
                return UIIcons.getIcon(7);

            case ARMOR:
                return UIIcons.getIcon(11);

            case AMULET:
                return UIIcons.getIcon(9);

            case HELMET:
                return UIIcons.getIcon(8);

            case CLOAK:
                return UIIcons.getIcon(13);

            case SHOULDERS:
                return UIIcons.getIcon(10);

            case GLOVES:
                return UIIcons.getIcon(12);

            case PANTS:
                return UIIcons.getIcon(14);

            case SHOES:
                return UIIcons.getIcon(15);

            case RING:
                return UIIcons.getIcon(16);

            default:
                return UIIcons.getIcon(17);
        }

    }

}
