package com.thetale.app.enums;

import android.graphics.Color;

import com.thetale.app.R;

/**
 * Created by Rim4i4ok on 14.06.2014.
 */
public class EnumRarity {

    private static final int START = 0;

    public static final int TRASH = 0;
    public static final int RARE = 1;
    public static final int EPIC = 2;

    private static final int END = 2;

    private static String[] description = { "обычный артефакт"
                                          , "редкий артефакт"
                                          , "эпический артефакт"  };

    public static String getRarity(int rId){
        if(rId >= START && rId <= END){
            return description[rId];
        }
        else{
            return "Неизвестно";
        }
    }

    public static int getColor(int rId){

        int result = R.color.color_trash;

        switch (rId) {
            case TRASH:
                result = R.color.color_trash;
                break;

            case RARE:
                result = R.color.color_rare;
                break;

            case EPIC:
                result = R.color.color_epic;
                break;

            default:
                result = R.color.color_trash;
                break;
        }
        return  result;
    }

}
