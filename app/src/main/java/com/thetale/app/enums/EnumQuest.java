package com.thetale.app.enums;

import android.graphics.Bitmap;

import com.thetale.app.images.UIMap;
import com.thetale.app.images.UIQuests;

/**
 * Created by Rim4i4ok on 5/3/14.
 */
public class EnumQuest {

    public static final int HELP_FRIEND = 1;
    private static final String HELP_FRIEND_STR = "help_friend";

    public static final int NEXT_SPENDING = 2;
    private static final String NEXT_SPENDING_STR = "next-spending";

    public static final int SPYING = 3;
    private static final String SPYING_STR = "spying";

    public static final int DELIVERY = 4;
    private static final String DELIVERY_STR = "delivery";

    public static final int NO_QUEST = 5;
    private static final String NO_QUEST_STR = "no-quest";

    public static final int HOMETOWN = 6;
    private static final String HOMETOWN_STR = "hometown";

    public static final int COLLECT_DEBT = 7;
    private static final String COLLECT_DEBT_STR = "collect_debt";

    public static final int PILGRIMAGE = 8;
    private static final String PILGRIMAGE_STR = "pilgrimage";

    public static final int HELP = 9;
    private static final String HELP_STR = "help";

    public static final int CARAVAN = 10;
    private static final String CARAVAN_STR = "caravan";

    public static final int SEARCH_SMITH = 11;
    private static final String SEARCH_SMITH_STR = "search_smith";

    public static final int UNKNOWN = 12;

    // Преобразование текстового обозначения квеста к справонику
    private static int StringToInt(String type) {

        if (HELP_FRIEND_STR.equalsIgnoreCase(type)) {
            return HELP_FRIEND;
        } else if (NEXT_SPENDING_STR.equalsIgnoreCase(type)) {
            return NEXT_SPENDING;
        } else if (SPYING_STR.equalsIgnoreCase(type)) {
            return SPYING;
        } else if (DELIVERY_STR.equalsIgnoreCase(type)) {
            return DELIVERY;
        } else if (NO_QUEST_STR.equalsIgnoreCase(type)) {
            return NO_QUEST;
        } else if (HOMETOWN_STR.equalsIgnoreCase(type)) {
            return HOMETOWN;
        } else if (COLLECT_DEBT_STR.equalsIgnoreCase(type)) {
            return COLLECT_DEBT;
        } else if (PILGRIMAGE_STR.equalsIgnoreCase(type)) {
            return PILGRIMAGE;
        } else if (HELP_STR.equalsIgnoreCase(type))
            return HELP;
        else if (CARAVAN_STR.equalsIgnoreCase(type)) {
            return CARAVAN;
        } else if (SEARCH_SMITH_STR.equalsIgnoreCase(type)) {
            return SEARCH_SMITH;
        } else {
            return UNKNOWN;
        }
    }

    // Получение картинки экипировки
    public static Bitmap getImage(String type){

        switch (StringToInt(type)){
            case HELP_FRIEND:
                return UIQuests.getIcon(3);

            case NEXT_SPENDING:
                return UIQuests.getIcon(11);

            case SPYING:
                return UIQuests.getIcon(8);

            case DELIVERY:
                return UIQuests.getIcon(1);

            case NO_QUEST:
                return UIQuests.getIcon(10);

            case HOMETOWN:
                return UIQuests.getIcon(4);

            case COLLECT_DEBT:
                return UIQuests.getIcon(7);

            case PILGRIMAGE:
                return UIQuests.getIcon(17);

            case HELP:
                return UIQuests.getIcon(2);

            case UNKNOWN:
                return UIQuests.getIcon(12);

            case CARAVAN:
                return  UIQuests.getIcon(1);

            case SEARCH_SMITH:
                return UIQuests.getIcon(9);

            default:
                return UIQuests.getIcon(12);
        }

    }
}
