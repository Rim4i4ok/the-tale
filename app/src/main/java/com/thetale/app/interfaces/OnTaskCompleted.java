package com.thetale.app.interfaces;

/**
 * Created by Rim4i4ok on 4/1/14.
 */
public interface OnTaskCompleted {
    void onTaskCompleted(Boolean value);
}
