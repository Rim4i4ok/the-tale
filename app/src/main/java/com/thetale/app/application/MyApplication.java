package com.thetale.app.application;

import android.app.Application;
import android.content.res.Configuration;
import android.graphics.Bitmap;

import com.thetale.app.http.HttpClientFactory;

import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

import java.util.List;

/**
 * Created by Rim4i4ok on 3/22/14.
 */
public class MyApplication {

    private static DefaultHttpClient httpClient = HttpClientFactory.getThreadSafeClient();

    private static BasicHttpContext httpContext = new BasicHttpContext();

    private static CookieStore cookieStore = new BasicCookieStore();
    public static void setCookieStore(CookieStore cookieStore) {
        MyApplication.cookieStore = cookieStore;
    }

    public MyApplication(){
        Object store =  httpContext.getAttribute(ClientContext.COOKIE_STORE);
        if(store == null){
            httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
        }
    }

    private static CookieStore _cookieStore;
    public static void set_cookieStore(CookieStore cookieStore) { _cookieStore = cookieStore; }

    // Токен
    public static String getToken(){
        String result = "";

        List<Cookie> cookies = cookieStore.getCookies();
        for (int i = 0; i < cookies.size(); i++){
            Cookie iterator = cookies.get(i);
            if(iterator.getName().equalsIgnoreCase("csrftoken")){
                result = iterator.getValue();
            }
        }

        return result;
    }

    public DefaultHttpClient getHttpClient(){
        return  httpClient;
    }

    public DefaultHttpClient getNewHttpClient(){
        httpClient = HttpClientFactory.getNewClient();
        return httpClient;
    }

    public BasicHttpContext getHttpContext(){
        return httpContext;
    }

    public CookieStore getCookieStore(){
        return cookieStore;
    }

}
