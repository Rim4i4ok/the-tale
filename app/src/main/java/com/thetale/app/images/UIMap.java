package com.thetale.app.images;

import android.graphics.Bitmap;

/**
 * Created by Rim4i4ok on 18.05.2014.
 */
public class UIMap {

    // Картинка с иконками квеста
    private static Bitmap icons;
    public static void setIcons(Bitmap bitmap) {
        UIMap.icons = bitmap;
    }
    public static Bitmap getIcons() {
        return icons;
    }

    // Количество столбцов на спрайте
    private static int columnCount;
    public static void setColumnCount(int columnCount) {
        UIMap.columnCount = columnCount;
    }

    // Количество строк на спрайте
    private  static int rowCount;
    public static void setRowCount(int rowCount) {
        UIMap.rowCount = rowCount;
    }

    // Получение конкретной иконки из спрайта
    public static Bitmap getIcon(int icon){
        // высота иконки
        int iconHeight = icons.getHeight() / rowCount;
        // ширина иконки
        int iconWidth = icons.getWidth() / columnCount;

        return Bitmap.createBitmap(icons
                                 , icon % columnCount * iconWidth
                                 , icon / columnCount * iconHeight
                                 , iconWidth
                                 , iconHeight
                                 , null
                                 , false);
    }

}
