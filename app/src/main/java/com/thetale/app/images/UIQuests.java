package com.thetale.app.images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by Rim4i4ok on 4/20/14.
 */
public class UIQuests {

    // Картинка с иконками квеста
    private static Bitmap icons;
    public static void setIcons(Bitmap bitmap) {
        UIQuests.icons = bitmap;
    }
    public static Bitmap getIcons() {
        return icons;
    }

    // Количество столбцов на спрайте
    private static int columnCount;
    public static void setColumnCount(int columnCount) {
        UIQuests.columnCount = columnCount;
    }

    // Количество строк на спрайте
    private  static int rowCount;
    public static void setRowCount(int rowCount) {
        UIQuests.rowCount = rowCount;
    }

    // Получение конкретной иконки из спрайта
    public static Bitmap getIcon(int icon){
        // высота иконки
        int iconHeight = icons.getHeight() / rowCount;
        // ширина иконки
        int iconWidth = icons.getWidth() / columnCount;

        if(icon < rowCount){
            return Bitmap.createBitmap(icons, 0, iconHeight * icon, iconWidth, iconHeight, null, false);
        }
        else{
            return Bitmap.createBitmap(icons, iconWidth, iconHeight * (icon - rowCount), iconWidth, iconHeight, null, false);
        }
    }

}
