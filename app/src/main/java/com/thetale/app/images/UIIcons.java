package com.thetale.app.images;

import android.graphics.Bitmap;

/**
 * Created by Rim4i4ok on 4/12/14.
 */
public class UIIcons {

    // Картинка с иконками
    private static Bitmap icons;
    public static void setIcons(Bitmap bitmap) {
        UIIcons.icons = bitmap;
    }
    public static Bitmap getIcons() {
        return icons;
    }

    // Количество иконок на картинке
    private static int imagesCount;
    public static void setImagesCount(int imagesCount) {
        UIIcons.imagesCount = imagesCount;
    }

    // Получение конкретной иконки из изображения
    public static Bitmap getIcon(int icon){
        int iconHeight = icons.getHeight() / imagesCount;
        return Bitmap.createBitmap(icons, 0,iconHeight * icon, icons.getWidth(), iconHeight, null, false);
    }


}
